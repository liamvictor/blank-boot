<?php
$config['mode'] = "production";
// $config['mode']      = "dev";
$config['site_name'] = NULL;
// $config['site_name'] = "Liam's Lovely Site";
$config['mail']['mail_to']      = 'liamvictor@gmail.com';
$config['mail']['mail_to_name'] = "Liam Delahunty";
$config['mail']['Host']         = "localhost";
$config['mail']['Port']         = "587";
$config['mail']['SMTPSecure']   = "tls";
$config['mail']['Username']     = NULL;
$config['mail']['Password']     = NULL;
$config['mail']['setFrom']      = NULL;
$config['mail']['isHTML']       = "true";
$config['mail']['phpmailer']    = "/vendor/phpmailer/PHPMailerAutoload.php";