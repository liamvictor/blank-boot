<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */

// SMTP needs accurate times, and the PHP time zone MUST be set
// This should be done in your php.ini, but this is how to do it if you don't have access to that
// date_default_timezone_set('Etc/UTC');

define('INC_ROOT', dirname($_SERVER['DOCUMENT_ROOT']));
// require_once INC_ROOT . "/vendor/phpmailer/phpmailer/PHPMailerAutoload.php";
require_once "./phpmailer/PHPMailerAutoload.php";

// Create a new PHPMailer instance
$mail = new PHPMailer;
// Tell PHPMailer to use SMTP
$mail->isSMTP();
// Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
// Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

$mail->SMTPOptions = array(
	'ssl' => array(
		'verify_peer'       => false,
		'verify_peer_name'  => false,
		'allow_self_signed' => true,
	),
);

// Set the hostname of the mail server
// $mail->Host = '4-parents.co.uk';
$mail->Host = 'localhost';

// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
// Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
// Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
// Whether to use SMTP authentication
$mail->SMTPAuth = false;
// Username to use for SMTP authentication - use full email address for gmail
// $mail->Username = "admin@4-parents.co.uk";
// $mail->Password = "adminTCFparent432321";
// $mail->Username = "parentsuk";
// $mail->Password = "TCFparent432321";

// Set who the message is to be sent from
$mail->setFrom('admin@4-parents.co.uk', 'Admin - 4-parents.co.uk');
// Set an alternative reply-to address
// $mail->addReplyTo('liam@onlinesales.co.uk', 'Liam onlinesales');
// Set who the message is to be sent to
$mail->addAddress('liamvictor@gmail.com', 'Liam Victor');
// Set the subject line
$mail->Subject = 'PHPMailer SMTP test - local' . phpversion();

// Read an HTML message body from an external file, convert referenced images to embedded,
// convert HTML into a basic plain-text alternative body
// $mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->Body = "This is the HTML body of the email. \nUsing localhost.\nNo smtp a/c\SMTPAuth = false;\n" . phpversion();

// Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
// Attach an image file
// $mail->addAttachment('images/phpmailer_mini.png');
// send the message, check for errors
if (!$mail->send()) {
	echo "Mailer Error: " . $mail->ErrorInfo;
} else {
	echo "Message sent!";
}

/*
|--------------------------------------------------------------------------
Here is your client ID
97753236829-fpv5it5rjko9t1fqugvhnc66k5sktj37.apps.googleusercontent.com
Here is your client secret
lhAhZOAvzLuVykBN2Lw_l8Or
|--------------------------------------------------------------------------
 */
