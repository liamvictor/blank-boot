<?php
define('INC_ROOT', dirname($_SERVER['DOCUMENT_ROOT']));
if (file_exists(INC_ROOT . "/app/config.php")) {
	include INC_ROOT . "/app/config.php";
} else {
	$config['mode'] = "dev";
}
include "page/functions.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?=$site_name;?> Site Map</title>
    <link rel="canonical" href="<?=$canonical;?>">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="page/style.css">
  </head>

  <body>

<?php include "page/navbar.php";?>

    <div class="container" role="main">

      <div class="page-header">
        <h1>Site Map</h1>
      </div>

      <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/contact">Contact</a></li>
          <li><a href="/links">Links</a></li>
          <li><a href="/privacy">Privacy Policy</a></li>
          <li><a href="/site-map">Site Map</a></li>
      </ul>


    </div> <!-- /container -->

<?php include "page/footer.php";?>



  </body>
</html>
