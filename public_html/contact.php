<?php
define('INC_ROOT', dirname($_SERVER['DOCUMENT_ROOT']));
if (file_exists(INC_ROOT . "/app/config.php")) {
	include INC_ROOT . "/app/config.php";
} else {
	$config['mode'] = "dev";
}
include "page/functions.php";

if (isset($_POST['submit'])) {
	if (empty($_POST['thieremail'])) {
		$errors[]   = 'Please enter your email';
		$thieremail = NULL;
	} else {
		$thieremail = trim($_POST['thieremail']);
		if (!filter_var($thieremail, FILTER_VALIDATE_EMAIL)) {
			$errors[] = 'Sorry, your email doesn\'t appear to be valid';
			// filter it!
		}
		$thieremail = filter_var($thieremail, FILTER_VALIDATE_EMAIL);
	}

	if (empty($_POST['thiername'])) {
		$errors[]  = 'Please enter your name';
		$thiername = NULL;
	} else {
		$thiername = trim($_POST['thiername']);
		if (!filter_var($thiername, FILTER_SANITIZE_STRING)) {
			$errors[] = 'Sorry, your name isn\'t a valid string.';
			// filter it!
		}
		$thiername = filter_var($thiername, FILTER_SANITIZE_STRING);
	}
	if (empty($_POST['message'])) {
		$errors[] = 'Please enter a message';
		$message  = NULL;
	} else {
		$message = trim($_POST['message']);
		if (!filter_var($message, FILTER_SANITIZE_STRING)) {
			$errors[] = 'Sorry, your message isn\'t a valid string.';
			// filter it!
		}
		$message = filter_var($message, FILTER_SANITIZE_STRING);
	}
} else {
	$thieremail = $thiername = $message = NULL;
}

if (isset($_POST['submit']) && !isset($errors)) {
	/*
		|--------------------------------------------------------------------------
		| Reseller package doesn't support mail()
		| Use PHPmailer
		|--------------------------------------------------------------------------
	*/

	if (empty($config['mail']['phpmailer'])) {
		require_once "./phpmailer/PHPMailerAutoload.php";
	} else {
		require_once INC_ROOT . $config['mail']['phpmailer'];
	}

	// Create a new PHPMailer instance
	$mail = new PHPMailer;
	$mail->IsSMTP();

	if ($config['mode'] != "production") {
		// test please.
		$mail->SMTPDebug   = 2;
		$mail->Debugoutput = 'html';
	}

	if (empty($config['mail']['mail_to'])) {
		$mail_to = "admin@" . $domain_name;
	} else {
		$mail_to = $config['mail']['mail_to'];
	}
	if (empty($config['mail']['mail_to_name'])) {
		$mail_to_name = $site_name;
	} else {
		$mail_to_name = $config['mail']['mail_to_name'];
	}

	$subject     = 'New Mail from ' . $domain_name;
	$new_message = 'From: ' . $thiername . PHP_EOL . PHP_EOL;
	$new_message .= 'From: ' . $thieremail . PHP_EOL . PHP_EOL;
	$new_message .= "Message:" . PHP_EOL . $message . PHP_EOL . PHP_EOL;
	$new_message .= $_SERVER['HTTP_HOST'] . PHP_EOL . PHP_EOL;
	$html_message = nl2br($new_message);

//  cert bug in PHP 5.6+
	$mail->SMTPOptions = array(
		'ssl' => array(
			'verify_peer'       => false,
			'verify_peer_name'  => false,
			'allow_self_signed' => true,
		),
	);

	if (!empty($config['mail']['Host'])) {
		$mail->Host = $config['mail']['Host'];
	} else {
		$mail->Host = "localhost";
	}

	// Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	if (!empty($config['mail']['Port'])) {
		$mail->Port = $config['mail']['Port'];
	} else {
		$mail->Port = 587;
	}

	// Set the encryption system to use - ssl (deprecated) or tls
	if (!empty($config['mail']['SMTPSecure'])) {
		$mail->SMTPSecure = $config['mail']['SMTPSecure'];
	} else {
		$mail->SMTPSecure = 'tls';
	}

	// Whether to use SMTP authentication
	if (!empty($config['mail']['Username'])) {
		$mail->Username = $config['mail']['Username'];
	}

	if (!empty($config['mail']['Password'])) {
		$mail->Password = $config['mail']['Password'];
	}

	if (!empty($config['mail']['setFrom'])) {
		$mail->setFrom = $config['mail']['setFrom'];
	} else {
		$setFrom = "admin@" . $domain_name;
	}

	// Set who the message is to be sent from
	$mail->setFrom($setFrom, $site_name);
	// Set an alternative reply-to address
	// $mail->addReplyTo('admin@theclientfactory.co.uk', 'do not reply');
	// Set who the message is to be sent to
	$mail->addAddress($mail_to, $mail_to_name);
	$mail->Subject = $subject;
	// Read an HTML message body from an external file, convert referenced images to embedded,
	// convert HTML into a basic plain-text alternative body

	$mail->msgHTML($html_message);
	// Replace the plain text body with one created manually
	$mail->isHTML(true);
	$mail->AltBody = $new_message;

	if (!$mail->send()) {
		$errors[] = $mail->ErrorInfo;
	} else {
		$sent = 1;
	}

}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?=$site_name;?> Contact</title>
    <link rel="canonical" href="<?=$canonical;?>">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="page/style.css">
  </head>

  <body>

<?php include "page/navbar.php";?>

    <div class="container theme-showcase" role="main">

      <div class="page-header">
        <h1>Contact <?=$site_name;?></h1>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST">

<?php

if (isset($_POST['submit'])) {
	if (is_array($errors)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">Please fix the following issues:</div>";
		echo "<ul>";
		foreach ($errors AS $error) {
			echo "<li>" . $error . "</li>" . PHP_EOL;
		}
		echo "</ul>";
	} elseif (isset($sent) && $sent == 1) {
		echo "<div class=\"alert alert-success\" role=\"alert\">Message sent. </div>";
	} else {
		echo "<div class=\"alert alert-warning\" role=\"alert\">Unknown Issue. Sorry. </div>";
	}
} else {
	echo "<div class=\"alert alert-info\" role=\"alert\">All fields are required. </div>";
}

?>

            <div class="form-group">
              <label for="thieremail">Email address</label>
              <input type="email" class="form-control" id="thieremail" name="thieremail" placeholder="Email" value="<?=$thieremail;?>">
            </div>
            <div class="form-group">
              <label for="thiername">Your Name</label>
              <input type="text" class="form-control" id="thiername"  name="thiername" placeholder="Your Name" value="<?=$thiername;?>">
            </div>
            <div class="form-group">
              <label for="message">Your Message</label>
              <textarea class="form-control" rows="3" name="message"><?=$message;?></textarea>
            </div>

<?php
if (!isset($sent)) {
	echo "<button type=\"submit\" class=\"btn btn-default\" name=\"submit\">Submit</button>" . PHP_EOL;
}
?>
          </form>
        </div>
      </div>

    </div> <!-- /container -->

<?php include "page/footer.php";?>


  </body>
</html>
