<?php
function getDomain($url) {
	global $domains, $subs;
	//  this gets the actual host domain
	//  not like the PHP hosts which includes the subdomain.
	if (!preg_match("/^https?:\/\//", $url)) {
		$url = "http://" . $url;
	}
	$pieces          = parse_url($url);
	$domain          = isset($pieces['host']) ? $pieces['host'] : '';
	$subs[$domain][] = $url;
	if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
		$realHost = $regs['domain'];
		if (isset($domains[$realHost])) {
			$domains[$realHost] = $domains[$realHost] + 1;
		} else {
			$domains[$realHost] = 1;
		}
		return $regs['domain'];
	}
	return false;
}
/*
|--------------------------------------------------------------------------
| DEFAULTS
|--------------------------------------------------------------------------
 */

// Site name is English
// Domain Name is a domainname.tld
if (!empty($config['site_name'])) {
	$site_name = $config['site_name'];
} else {
	$site_name = getDomain($_SERVER['HTTP_HOST']);
}
// used for creating email
if (!empty($config['domain_name'])) {
	$domain_name = $config['domain_name'];
} else {
	$domain_name = getDomain($_SERVER['HTTP_HOST']);
}

if (empty($site_name)) {$site_name = "BLANK";}
if (empty($domain_name)) {$domain_name = "TEST.TLD";}
$canonical = $active = NULL;
if (isset($_SERVER['PHP_SELF'])) {
	$active = basename($_SERVER['PHP_SELF'], ".php");
}
if (!isset($_SERVER['REQUEST_SCHEME'])) {
	if (!isset($_SERVER['HTTPS'])) {
		$request_scheme = "http";
	} else {
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
			$request_scheme = "https";
		} else {
			$request_scheme = "http";
		}
	}
} else {
	$request_scheme = $_SERVER['REQUEST_SCHEME'];
}

if (preg_match('/^www\./', $_SERVER['SERVER_NAME'])) {
	$canonical = $domain = $request_scheme . "://" . $_SERVER['SERVER_NAME'] . "/";
} else {
	$canonical = $domain = $request_scheme . "://www." . $_SERVER['SERVER_NAME'] . "/";
}
if (isset($active) && $active != "index") {
	$canonical = $domain . $active;
}
?>