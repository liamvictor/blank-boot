    <footer class="footer">
      <div class="container">
        <p class="text-muted">
        <p id='copyright'>&copy; <?php echo date("Y"); ?> <?=$site_name;?> All Rights Reserved</p>

        <ul class="list-inline">
            <li class="list-inline-item"><a href="/">Home</a></li>
            <li class="list-inline-item"><a href="/contact">Contact</a></li>
            <li class="list-inline-item"><a href="/privacy">Privacy Policy</a></li>
            <li class="list-inline-item"><a href="/site-map">Site Map</a></li>
        </ul>

      </div>
    </footer>


        <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

