<?php
define('INC_ROOT', dirname($_SERVER['DOCUMENT_ROOT']));
if (file_exists(INC_ROOT . "/app/config.php")) {
	include INC_ROOT . "/app/config.php";
} else {
	$config['mode'] = "dev";
}
include "page/functions.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title><?=$site_name;?> Privacy Policy</title>
    <link rel="canonical" href="<?=$canonical;?>">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="page/style.css">
  </head>

  <body>

<?php include "page/navbar.php";?>

    <div class="container" role="main">

      <div class="page-header">
        <h1>Privacy Policy</h1>
      </div>


<h2>The privacy of our visitors to <?=$domain_name;?> is important to us.</h2>

<p>At <?=$site_name;?>, we recognise that privacy of your personal information is important.
Here is information on what types of personal information we receive and collect when you use and visit <?=$domain_name;?>, and how we safeguard your information.
We never sell your personal information to third parties.</p>

<h3>Log Files</h3>
<p>As with most other websites, we collect and use the data contained in log files.
The information in the log files include your IP (internet protocol) address, your ISP (internet service provider), the browser you used to visit our site (such as Internet Explorer, Chrome or Firefox), the time you visited our site and which pages you visited throughout our site.</p>

<h3>Cookies and Web Beacons</h3>
<p>We do use cookies to store information, such as your personal preferences when you visit our site.
This could include only showing you some information once during your visit, or the ability to login to some of our features.</p>

<p>We may also use third party advertisements on <?=$domain_name;?> to support our site.
Some of these advertisers may use technology such as cookies and web beacons when they advertise on our site, which will also send these advertisers (such as Google through the Google AdSense program) information including your IP address and your ISP.
This information is generally used for geotargeting purposes or showing certain ads based on specific sites visited.</p>

<h3>Control</h3>
<p>You can chose to disable or selectively turn off our cookies or third-party cookies in your browser settings
However, this can affect how you are able to interact with our site as well as other websites.
This could include the inability to login to services, such as logging into social media, forums, or other accounts.</p>

<p>Deleting cookies does not mean you are permanently opted out of any advertising program.
Unless you have settings that disallow cookies, the next time you visit a site running the advertisements, a new cookie will be added.</p>


    </div> <!-- /container -->

<?php include "page/footer.php";?>



  </body>
</html>
